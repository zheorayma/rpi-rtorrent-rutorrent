#!/bin/bash

MEDIAINFO_VER=0.7.82

set -x && \

build_pkgs="build-base git libtool automake autoconf wget subversion cppunit-dev openssl-dev c-ares-dev libxml2-dev ncurses-dev curl-dev" && \
runtime_pkgs="nginx php5 php5-cli php5-fpm php5-xmlrpc c-ares ffmpeg ca-certificates curl openssl gzip zip unrar supervisor geoip" && \

apk --no-cache add ${build_pkgs} ${runtime_pkgs} && \
cd /tmp && \

wget http://mediaarea.net/download/binary/mediainfo/${MEDIAINFO_VER}/MediaInfo_CLI_${MEDIAINFO_VER}_GNU_FromSource.tar.gz && \
wget http://mediaarea.net/download/binary/libmediainfo0/${MEDIAINFO_VER}/MediaInfo_DLL_${MEDIAINFO_VER}_GNU_FromSource.tar.gz && \
tar xzf MediaInfo_DLL_${MEDIAINFO_VER}_GNU_FromSource.tar.gz && \
tar xzf MediaInfo_CLI_${MEDIAINFO_VER}_GNU_FromSource.tar.gz && \

cd  /tmp/MediaInfo_DLL_GNU_FromSource && \
./SO_Compile.sh && \
cd /tmp/MediaInfo_DLL_GNU_FromSource/ZenLib/Project/GNU/Library && \
make install && \
cd /tmp/MediaInfo_DLL_GNU_FromSource/MediaInfoLib/Project/GNU/Library && \
make install && \
cd /tmp/MediaInfo_CLI_GNU_FromSource && \
./CLI_Compile.sh && \
cd /tmp/MediaInfo_CLI_GNU_FromSource/MediaInfo/Project/GNU/CLI && \
make install && \
ldconfig
