#!/bin/bash

apk update

# Compile mediainfo
cp mediainfo/compileMediainfo.sh /root/config/compileMediainfo.sh
#./root/config/compileMediainfo.sh

# Compile rtorrent
cp rtorrent/compileRtorrent.sh /root/config/compileRtorrent.sh 
#./root/config/compileRtorrent.sh

# Download base for ruTorrent 
cp rutorrent/getRutorrent.sh /root/config/getRutorrent.sh
#./root/config/getRutorrent.sh

# rtorrent and rutorrent startup 
cp rtorrent/runRtorrent.sh /root/config/runRtorrent.sh
cp rutorrent/runRutorrent.sh /root/config/runRutorrent.sh

# rtorrent and ruTorrent base configurations
cp rtorrent/config/.rtorrent.rc /root/config/.rtorrent.rc
cp rutorrent/config/config.php /root/config/config.php

# Base configuration for NGinx
cp nginx/config/rutorrent.conf     /etc/nginx/sites-enabled/default
cp nginx/config/rutorrent-ssl.conf /etc/nginx/sites-enabled/tls
cp nginx/config/nginx.conf         /etc/nginx/nginx.conf

# Configure rtorrent user
useradd -d /home/rtorrent -m -s /bin/bash rtorrent
chown -R rtorrent:rtorrent /home/rtorrent

# Geo Codes
curl -LOks http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz && gunzip GeoLiteCity.dat.gz && \
mkdir /usr/share/GeoIP && mv GeoLiteCity.dat /usr/share/GeoIP/GeoIPCity.dat

# Cleanup repositories
apk del ${build_pkgs}
apk del --purge build-dependencies
rm -rf /var/cache/apk/* /var/tmp/* /tmp/*
deluser svn
delgroup svnusers

# Configure supervisor
cp supervisord/supervisord.conf /etc/supervisor/supervisord.conf
