#!/bin/bash

RTORRENT_VER=0.9.6
LIBTORRENT_VER=0.13.6

set -x && \

build_pkgs="build-base git libtool automake autoconf wget subversion cppunit-dev openssl-dev c-ares-dev libxml2-dev ncurses-dev curl-dev" && \
runtime_pkgs="nginx php5 php5-cli php5-fpm php5-xmlrpc c-ares ffmpeg ca-certificates curl openssl gzip zip unrar supervisor geoip" && \

apk --no-cache add ${build_pkgs} ${runtime_pkgs} && \
cd /tmp && \
wget http://curl.haxx.se/download/curl-7.39.0.tar.gz && \
tar xzvfp curl-7.39.0.tar.gz && \
cd curl-7.39.0 && \
./configure --enable-ares --enable-tls-srp --enable-gnu-tls --with-zlib --with-ssl && \
make -j 2 && \
make install && \
cd .. && \
rm -rf curl-* && \
ldconfig && \

svn --non-interactive --trust-server-cert checkout http://svn.code.sf.net/p/xmlrpc-c/code/stable xmlrpc-c && \
cd xmlrpc-c && \
./configure --enable-libxml2-backend --disable-abyss-server --disable-cgi-server && \
make -j && \
make install && \
cd .. && \
rm -rf xmlrpc-c && \
ldconfig && \

git clone https://github.com/rakshasa/libtorrent.git && \
cd libtorrent && \
git checkout ${LIBTORRENT_VER} && \
./autogen.sh && \
./configure --disable-instrumentation && \
make -j 2 && \
make install && \
cd .. && \
rm -rf libtorrent-* && \
ldconfig && \

git clone https://github.com/rakshasa/rtorrent.git && \
cd rtorrent && \
git checkout ${RTORRENT_VER} && \
./autogen.sh && \
./configure --with-xmlrpc-c --with-ncurses && \
make -j 2 && \
make install && \
cd .. && \
rm -rf rtorrent-* && \
ldconfig
