#!/bin/bash

build_pkgs="build-base git libtool automake autoconf wget subversion cppunit-dev openssl-dev ncurses-dev curl-dev"
runtime_pkgs="nginx php5 php5-cli php5-fpm php5-xmlrpc ffmpeg ca-certificates curl openssl gzip zip unrar supervisor geoip"

apk del ${build_pkgs}
apk del ${runtime_pkgs}
apk del --purge build-dependencies
rm -rf /var/cache/apk/* /var/tmp/* /tmp/*
deluser svn
delgroup svnusers