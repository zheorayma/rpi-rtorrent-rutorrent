#!/bin/bash

cp ../config.php /var/www/rutorrent/conf/

chmod +x ../startup-rtorrent.sh && cp ../startup-rtorrent.sh /root/ 
chmod +x ../startup-nginx.sh && cp ../startup-nginx.sh /root/ 
chmod +x ../startup-php.sh && cp ../startup-php.sh /root/
cp ../.rtorrent.rc /root/

mkdir -p /etc/supervisor.d
cp ../supervisord.conf /etc/supervisor.d/supervisord.ini

mkdir -p /etc/nginx/sites-enabled
cp ../nginx.conf /etc/nginx/
cp ../php-fpm.conf /etc/php5/
cp ../rutorrent-*.nginx /root/