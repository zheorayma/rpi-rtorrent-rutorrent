#!/bin/bash

MEDIAINFO_VER=0.7.82
RTORRENT_VER=0.9.6
LIBTORRENT_VER=0.13.6

build_pkgs="build-base git libtool automake autoconf wget subversion cppunit-dev openssl-dev ncurses-dev curl-dev"
runtime_pkgs="nginx php5 php5-cli php5-fpm php5-xmlrpc ffmpeg ca-certificates curl gzip zip unrar supervisor geoip"

apk --no-cache add ${build_pkgs} ${runtime_pkgs}
cd /tmp

git clone https://github.com/esmil/mktorrent
svn co http://svn.code.sf.net/p/xmlrpc-c/code/stable xmlrpc-c
git clone https://github.com/rakshasa/libtorrent.git
git clone https://github.com/rakshasa/rtorrent.git
wget http://mediaarea.net/download/binary/mediainfo/${MEDIAINFO_VER}/MediaInfo_CLI_${MEDIAINFO_VER}_GNU_FromSource.tar.gz
wget http://mediaarea.net/download/binary/libmediainfo0/${MEDIAINFO_VER}/MediaInfo_DLL_${MEDIAINFO_VER}_GNU_FromSource.tar.gz
tar xzf MediaInfo_DLL_${MEDIAINFO_VER}_GNU_FromSource.tar.gz
tar xzf MediaInfo_CLI_${MEDIAINFO_VER}_GNU_FromSource.tar.gz

cd /tmp/mktorrent
make -j 2
make install

cd  /tmp/MediaInfo_DLL_GNU_FromSource
./SO_Compile.sh
cd /tmp/MediaInfo_DLL_GNU_FromSource/ZenLib/Project/GNU/Library
make install
cd /tmp/MediaInfo_DLL_GNU_FromSource/MediaInfoLib/Project/GNU/Library
make install
cd /tmp/MediaInfo_CLI_GNU_FromSource
./CLI_Compile.sh
cd /tmp/MediaInfo_CLI_GNU_FromSource/MediaInfo/Project/GNU/CLI
make install

cd /tmp/xmlrpc-c
./configure --disable-libwww-client --disable-wininet-client --disable-abyss-server --disable-cgi-server
make -j 2
make install

cd /tmp/libtorrent
git checkout ${LIBTORRENT_VER}
./autogen.sh
./configure --disable-instrumentation --disable-debug
make -j 2
make install

cd /tmp/rtorrent
git checkout ${RTORRENT_VER}
./autogen.sh
./configure --with-xmlrpc-c
make -j 2
make install
ldconfig

mkdir -p /var/www && cd /var/www
git clone https://github.com/Novik/ruTorrent.git rutorrent
cd rutorrent && rm -rf .git
cd /var/www/rutorrent/plugins/
git clone https://github.com/astupidmoose/rutorrent-logoff.git logoff
cd logoff && rm -rf .git
cd /var/www/rutorrent/plugins/
git clone https://github.com/xombiemp/rutorrentMobile.git mobile
cd mobile && rm -rf .git
cd /var/www/rutorrent/plugins/
git clone https://github.com/Ardakilic/rutorrent-pausewebui.git pausewebui
cd pausewebui && rm -rf .git
cd /var/www/rutorrent/plugins/theme/themes
git clone https://github.com/Micdu70/QuickBox-Dark.git QuickBox-Dark
cd QuickBox-Dark && rm -rf .git
cd /

apk del ${build_pkgs}
apk del --purge build-dependencies
rm -rf /var/cache/apk/* /var/tmp/* /tmp/*
deluser svn
delgroup svnusers
